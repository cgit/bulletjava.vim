augroup BulletJava
  au!
  autocmd TextYankPost * lua require'bulletjava'.on_text_yanked()
  if has('rneovim')
    autocmd TextPutPost *.java lua require'bulletjava'.on_text_put(vim.v.event.regname)
  endif
augroup END

noremap <Plug>(bulletjava-put) p<cmd>lua require'bulletjava'.on_text_put(vim.v.register)<cr>
